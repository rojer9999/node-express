const validator = require('validator');

const { readData, writeData } = require('../repositories/user.repository');

class User {
  constructor(id, first_name, last_name, email, gender, ip_address){
    this.id = id;
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.gender = gender;
    this.ip_address = ip_address;
  }
}

const checkNewUser = (...args) => {
  for (const arg of args) {
    if (typeof arg !== 'string') return false;
  }
  const [ first_name, last_name, email, gender, ip ] = args;
  const { isEmail, isLength } = validator;
  const opt = { min: 3, max: 10 };
  const checkGender = gender === 'Male' || gender === 'Female';
  
  return (
    isEmail(email) &&
    isLength(first_name, opt) &&
    isLength(last_name, opt) &&
    checkGender &&
    isLength(ip, { min: 3 })
  );
}

const findUser = (id) => {
  return readData()
    .then(data => {
      let foundUser;

      for (let user of data) {
        if (user.id === +id){
          foundUser = user;
          break;
        }
      }
      
      return foundUser ? foundUser : null;
    });
}

const addUser = ({
  body: { first_name, last_name, email, gender },
  connection: { remoteAddress: ip }
}) => {
  return readData()
    .then(data => {
      if (checkNewUser(first_name, last_name, email, gender, ip)) {
        const id = data.length + 1;
        const newUser = new User(id, first_name, last_name, email, gender, ip);
        const newData = [...data, newUser];
        writeData(newData);

        return newUser;
      } else {
        return null;
      }
    });
}

const updateUser = ({
  body: { first_name, last_name, email, gender },
  connection: { remoteAddress: ip },
  params: { id }
}) => {
  return readData()
    .then(data => {
      const isInArr = () => {
        for (const el of data) {
          if (el.id == id) return true
        }
        return false;
      };
      if (
          checkNewUser(first_name, last_name, email, gender, ip) &&
          isInArr()
        ) {
        const newUser = new User(+id, first_name, last_name, email, gender, ip);
        console.log(newUser);
        const newData = data.map(user => user.id === +id ? newUser : user);
        writeData(newData);
        
        return newUser;
      } else {
        return null;
      }
    });
}

const deleteUser = (id) => {
  return readData()
    .then(data => {
      let deletedUser = null;
      const filteredArr = data.filter(user => {
        console.log('user.id :', typeof user.id, typeof id);
        if (user.id !== +id) {
          return true;
        } else {
          deletedUser = user;
          return false;
        }
      });
      writeData(filteredArr);
      
      return deletedUser ? deletedUser : null;
    });
}

module.exports = {
  findUser,
  addUser,
  updateUser,
  deleteUser
}