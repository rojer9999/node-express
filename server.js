const app = require('express')();
const bodyParser = require('body-parser');

const userRouter = require('./routes/user.js');
const indexRouter = require('./routes/index.js');

const { handleError } = require('./repositories/user.repository');

const PORT = process.env.PORT || 3000;
app
  .listen(PORT, () => console.log(`Server started on port ${PORT}`))
  .on('error', handleError);

app.use(bodyParser.json());

app.use('/user', userRouter);
app.use('/', indexRouter);
