const { createReadStream, createWriteStream } = require('fs');

const DATA_FILE = `${__dirname}/../innerData.json`;

const handleError = (error) => console.log(`WARNING! ${error}`);

const readData = () => {
  return new Promise(resolve => {
    let data = '';
    
    createReadStream(DATA_FILE, 'utf8')
      .on('data', (chunk) => data += chunk)
      .on('end', () => resolve(data))
      .on('error', (err) => console.log(err.code == 'ENOENT' ? "File wasn't found" : err));
  })
  .then((resolve) => JSON.parse(resolve))
  .catch(handleError);
}

const writeData = (data) => {
  return new Promise(resolve => {
    const str = JSON.stringify(data);

    const writeableStream = createWriteStream(DATA_FILE);
    writeableStream.write(str);
    writeableStream.end(resolve);
    writeableStream.on('error', (err) => {
      handleError(err);
      writeableStream.end();
    })
  })
  .catch(handleError);
}

module.exports = {
  readData,
  writeData,
  handleError
}