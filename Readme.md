# Readme

### Setup

1. `$ npm install`
2. Start server

### Use

- `$ npm start` - to run server
- `$ npm run dev` - to run server with nodemon

### Requests

**Get** an array of users:
```sh
  GET: /user
```

**Get** user with current ID:
```sh
  GET: /user/:id
```

**Create** a brand new user with schema:
`{
  first_name: 'str',
  last_name: 'str',
  email: 'str',
  gender: 'str'
}`
```sh
  POST: /user
```

**Update** user with data from request body with schema:
`{
  first_name: 'str',
  last_name: 'str',
  email: 'str',
  gender: 'str'
}`
```sh
  PUT: /user/:id
```

**Delete** user with current ID:
```sh
DELETE: /user/:id
```