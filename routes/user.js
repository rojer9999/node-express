const router = require('express').Router();

const { readData, handleError } = require('../repositories/user.repository');
const {
  findUser,
  addUser,
  updateUser,
  deleteUser
} = require('../services/user.service');

const validRes = (res, data) => {
  if (data) {
    res.send(data);
  } else {
    res.sendStatus(400);
    res.end();
  }
}

router.get('/', (req, res) => {
  readData()
    .then(users => res.send(users))
    .catch(handleError);
});

router.get('/:id', (req, res) => {
  findUser(req.params.id)
    .then((user) => validRes(res, user))
    .catch(handleError);
});

router.post('/', (req, res) => {
  addUser(req)
    .then((user) => validRes(res, user))
    .catch(handleError);
});

router.put('/:id', (req, res) => {
  updateUser(req)
  .then((user) => validRes(res, user))
  .catch(handleError);
})

router.delete('/:id', (req, res) => {
  deleteUser(req.params.id)
  .then((users) => validRes(res, users))
  .catch(handleError);
})

module.exports = router;